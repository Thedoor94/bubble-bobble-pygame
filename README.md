# Bubble Bobble

Replica del famoso arcade platform con protagonisti i due draghetti Bub e Bob capaci di sparare bolle per eliminare i nemici.
Il gioco è stato realizzato in Python e necessita della libreria grafica Pygame per funzionare.
Per installarla è sufficiente avere il gestore di pacchetti PIP nel proprio sistema e digitare "pip install pygame".

## Come avviare il gioco

Eseguire il file python "bubble_bobble_game.py".
Il gioco è multiplayer, ha un solo livello e si avranno a disposizione 3 vite prima di perdere definitivamente.
Le vite vengono perse se si viene toccati dai nemici e se si cade dalle piattaforme presenti nel gioco.
Lo scopo è quello di eliminare tutti i nemici a schermi colpendoli con le bolle che i draghetti possono sparare.

## Comandi di gioco

#### Comandi generali
* F4 = exit

#### Draghetto verde (Bub)

* freccia sinistra = sinistra
* freccia destra = destra
* freccia su = salto
* barra spaziatrice = sparo

#### Draghetto blu (Bob)

* a = sinistra
* d= destra 
* w= salto
* z = sparo
