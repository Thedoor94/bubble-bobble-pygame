
'''
@author Michele Tomaiuolo - http://www.ce.unipr.it/people/tomamic
@license This software is free - http://www.gnu.org/licenses/gpl.html
'''

import pygame
from arena import Character, Arena
from random import choice, randint


class Dragon(Character):
    
    W, H = 32, 32
    SPEED = 4
    JUMP, FLY = 30, 10
    LIFE=3

    def __init__(self, arena: Arena, x: int, y: int):
        self._x, self._y = x, y
        self._dx = 0
        self._jump = 0        # counts from JUMP to 0, when jumping
        self._landed = False  # has the dragon landed somwhere?
        self._arena = arena
        self._fly=False
        arena.add(self)
        

    def go_left(self):
        self._dx = -self.SPEED
        
    def go_right(self):
        self._dx = self.SPEED

    def stay(self):
        self._dx = 0

    def bubble (self, face_right:bool) :
        if (face_right == True) :
            Ball(self._arena, self._x+40, self._y, True)
        else :
            Ball(self._arena, self._x-40, self._y, False)

    def touch_down_arena (self):
        arena_w, arena_h = self._arena.size()
        if self._y > arena_h - self.H:
            self._y = arena_h - self.H
            self._landed = True
            self.LIFE-=1
            self._x= 160
            self._y=85

    def touch_up_arena (self):
        arena_w, arena_h = self._arena.size()
        if self._y <= 0:
            self._y = 0

    def control_life (self) :
        if (self.LIFE <=0):
            self._arena.remove(self)           

    def jump(self):
        if (self._fly==False) :
            self._jump = self.JUMP
            self._landed = False
            self._fly=True
        else :
            pass
        

    def move(self):
        self._x += self._dx

        if self._jump > 0:
            self._landed = False

            if self._jump > self.FLY:
                self._y -= self.SPEED
            self._jump -= 1
            
        elif not self._landed:
            self._y += self.SPEED
            
        #controlla se tocca il fondo dell'arena
        self.touch_down_arena()
        #controlla se tocca la cima dell'arena
        self.touch_up_arena()
        #controlla se il draghetto ha finito le vite
        self.control_life()
            
        self._landed=False
        
    def hit(self, other: Character):

        #scontro con il mostro
        if isinstance (other, Monster):
            e,f,g,h =other.rect()
            if ((e > self._x and e <(self._x+self.W)) and (f> self._y and f< (self._y +self.H))):
                self.LIFE-=1
                self._x= 160
                self._y=85

        #scontro con la piattaforma                
        if isinstance(other, Platform):
            a,b,c,d = other.rect()
            
            # se è fuori dalla piattaforma
            if(self._y+self.H > b+10):
                self._landed=False

                #gestione non oltrepassare muretto e puo attraversarlo da sotto
                if(a+c>self._x and self._x > a + c - 10):
                    self._x = a+c
                elif(a<self._x +self.W and self._x < a +10 - self.W):
                    self._x = a - self.W
                
            else:
                self._landed=True
                self._fly = False
                self._y = b-self.H
        
            
    def rect(self) -> (int, int, int, int):
        return self._x, self._y, self.W, self.H

    def symbol(self) -> int:
        return 0

class Ball(Character):
    
    W, H = 32, 32
    SPEED = 7
    OUT_OF_GAME = 1000  #valore per portare la bolla fuori dal campo di gioco una volta fatta sparire

    def __init__(self, arena: Arena, x: int, y: int, face_right:bool ):
        self._x, self._y = x, y
        self._dx = 0
        self._cont =0
        self._landed = False
        self._face_right= face_right
        self._arena = arena
        arena.add(self)
        

    def go_left(self):
        self._dx = -self.SPEED

    def touch_up_arena (self):
        touch = False
        arena_w, arena_h = self._arena.size()
        if self._y <= 0:
            self._y = 0
            touch==True
        return touch
    
        
    def go_right(self):
        self._dx = self.SPEED

    def stay(self):
        self._dx = 0

    def move(self):
        self._x += self._dx
       
        self._cont+=1
        if(self._cont > 12 and self.touch_up_arena()== False) :
            self._y-=4
            self._dx=0
        elif (self._cont< 12) :
            if (self._face_right == True) :
                self.go_right()
            else :
                self.go_left()
            
        
    def hit(self, other: Character):

        #scontro con la piattaforma                
        if isinstance(other, Platform):
            a,b,c,d = other.rect()
            
            # se è fuori dalla piattaforma
            if(self._y+self.H > b+10):
                self._landed=False

                #gestione non oltrepassare muretto
                if(a+c>self._x and self._x > a + c - 10):
                    self._x = a+c
                elif(a<self._x +self.W and self._x < a +10 - self.W):
                    self._x = a - self.W
                
            else:
                self._landed=True
                self._fly = False
                self._y = b-self.H

        #scontro con il draghetto 
        if isinstance(other, Dragon):
            self._arena.remove(self)
            self._x = self.OUT_OF_GAME

        #scontro con il mostro
        if isinstance (other, Monster):
            self._arena.remove(other)
            self._arena.remove(self)
            self._cont=12
                         
            
    def rect(self) -> (int, int, int, int) :
        return self._x, self._y, self.W, self.H

    def symbol(self) -> int:
      
        return 0


class Monster(Character):

    W, H = 30, 30
    SPEED = 3
    JUMP, FLY = 25, 2
    LIFE =1

    def __init__(self, arena: Arena, x: int, y: int):
        self._x, self._y = x, y
        self._dx =0
        self._jump = 0
        self._landed = False  
        self._arena = arena
        arena.add(self)
        self._v=False #sta cadendo dalla piattaforma?
        self._f=0 #contatore cicli move()
        

    def go_left(self):
        self._dx = -self._dx
        
    def go_right(self):
        self._dx = self._dx

    def stay(self):
        self._dx = 0
        
    def casual_movement(self):
        if (randint(0,20)<self._f<randint(20,40)):
            self.jump()          
        if (self._f < randint(70,100) and self._v == False) :
            self._dx = 3
        elif (self._f > randint(100,130) and self._f<160 and self._v == False):
             self._dx = -3        
        elif (self._f > randint(160,180)) :        
            self._f=0
        else :
             self._dx = 0

    def jump(self):
        if self._landed:
            self._jump = randint(self.JUMP, self.JUMP+10)
            self._landed = False

    def move(self):
        #contatore che aumenta casualmente
        self._f+=randint(0,2)
        
        self._x += self._dx

        if self._landed==False:
            self._dx=0

        if self._v==True :
             self._dx=0
                       
        if self._jump > 0:
            self._landed = False
            if self._jump > self.FLY:
                self._y -= self.SPEED
            self._jump -= 1
        elif not self._landed:
            self._y += self.SPEED

        arena_w, arena_h = self._arena.size()

        #movimento casuale mostro
        self.casual_movement()
            
        self._landed=False      

        arena_w, arena_h = self._arena.size()

        #tocca il fondo dell'arena, riappari in alto in centro
        if self._y > arena_h - self.H:
                                              
            self._x =arena_w / 2    #centro dell'arena
            self._y = 0
            self._dx=0
            self._landed = True
     
        
    def hit(self, other: Character):
        if isinstance(other, Platform):
            #dimensioni piattaforma
            a,b,c,d = other.rect()
            
            # se è fuori dalla piattaforma
            if(self._y+self.H > b+10):
                self._landed=False
                self._v=True

                #gestione non oltrepassare muretto
                if(a+c>self._x and self._x > a + c - 10):
                    self._x = a+c
                elif(a<self._x +self.W and self._x < a +10 - self.W):
                    self._x = a - self.W
                
            else:
                self._landed=True
                self._v=False
                self._y = b-self.H

        #scontro con la bolla
        if isinstance (other, Ball):
            self.LIFE =0
      

    def rect(self) -> (int, int, int, int):
        return self._x, self._y, self.W, self.H

    def symbol(self) -> int:
        return 0


class Platform(Character):
    def __init__(self, arena: Arena, x: int, y: int, w: int, h: int):
        self._x, self._y = x, y
        self._w, self._h = w, h
        self._arena = arena
        arena.add(self)

    def move(self):
        pass

    def hit(self, other: Character):
        pass
        
    def rect(self) -> (int, int, int, int):
        return self._x, self._y, self._w, self._h

    def symbol(self) -> int:
        return 0

