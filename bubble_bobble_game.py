
'''
@author Michele Tomaiuolo - http://www.ce.unipr.it/people/tomamic
@license This software is free - http://www.gnu.org/licenses/gpl.html
'''
import pygame
import time
from pygame.locals import ( MOUSEBUTTONDOWN, MOUSEBUTTONUP, MOUSEMOTION)
from arena import Arena, Character
from bubble_bobble import Dragon, Platform, Monster,Ball

#variabili

a=True #per ciclo infinito

playing = True
pos = 0
x_play=300
y_play=400
h_play=64
w_play=64

vite=3

arena = Arena(640, 480)
Platform(arena, 140, 90, 100, 30)
Platform(arena, 400, 90, 100, 30)
Platform(arena, 100, 180, 30, 60)
Platform(arena, 240, 160, 160, 30)
Platform(arena, 100, 240, 160, 30)
Platform(arena, 240, 320, 310, 30)
Platform(arena, 520, 270, 30, 50)
Platform(arena, 100, 380, 100, 30)

dragon = Dragon(arena, 160, 85)
dragon2 = Dragon (arena,110,350)

monster = Monster ( arena, 250, 85)
monster2 = Monster ( arena, 300, 70)
monster3 = Monster ( arena, 120, 75)
monster4 = Monster ( arena, 400, 80)

pygame.init()

clock = pygame.time.Clock()
screen = pygame.display.set_mode(arena.size())
dragon_img = pygame.image.load('dragon-0.png')
dragon2_img = pygame.image.load('dragonblu1.png')
monster_img = pygame.image.load('monster.png')
ball_img = pygame.image.load ('ball.png')
start_img = pygame.image.load ('start.png')
background_img = pygame.image.load ('background.jpg')
youwin_img = pygame.image.load ('youwin.png')
gameover_img = pygame.image.load ('game_over.jpg')

font= pygame.font.Font(None,26)

face_right_dragon = True
face_right_dragon2 = True

playing = False

pygame.mixer.init()
pygame.mixer.music.load('musica.mp3')
pygame.mixer.music.play(-1, 0.0)
pygame.mixer.music.set_volume(0.25)

while a :
    
    while playing== False :
            
        screen.fill((255, 255, 255))
        screen.blit(background_img, (0, 0))
        screen.blit(start_img, (x_play, y_play))
        play =font.render('Premi play per iniziare', True, (255,255,255))
        exit= font.render('Premi F4 per uscire', True, (255,255,255))
        screen.blit(play,(10,10))
        screen.blit(exit,(450,10))

        pygame.display.flip()

            
        for e in pygame.event.get():
       
            if e.type == MOUSEBUTTONDOWN and e.button == 1:
                if (x_play < pos[0] < (x_play+w_play) and  y_play< pos[1] < (y_play+h_play)):
                    start_img = pygame.image.load ('start_viola.png')
            elif e.type == MOUSEBUTTONUP and e.button == 1:
                if (x_play < pos[0] < (x_play+w_play) and  y_play< pos[1] < (y_play+h_play)):
                    start_img = pygame.image.load ('start.png')
                    playing=True
            elif e.type == MOUSEMOTION:
                pos= e.pos
            elif e.type == pygame.KEYDOWN:
                if e.key == pygame.K_F4:
                    print("Gioco terminato")
                    pygame.quit()
            
    while playing:
        for e in pygame.event.get():
            # print(e)
            if e.type == pygame.QUIT:
                playing = False
            elif e.type == pygame.KEYDOWN:
                if e.key == pygame.K_UP:
                    dragon.jump()
                elif e.key == pygame.K_SPACE:
                    dragon.bubble(face_right_dragon)
                elif e.key == pygame.K_LEFT:
                    face_right_dragon= False
                    dragon.go_left()
                    dragon_img = pygame.image.load('dragon-1.png')
                elif e.key == pygame.K_RIGHT:
                    face_right_dragon= True
                    dragon.go_right()
                    dragon_img = pygame.image.load('dragon-0.png')
            elif e.type == pygame.KEYUP:
                if e.key in (pygame.K_LEFT, pygame.K_RIGHT):
                    dragon.stay()
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_w:
                    dragon2.jump()
                elif e.key == pygame.K_z:
                    dragon2.bubble(face_right_dragon2)
                elif e.key == pygame.K_a:
                    face_right_dragon2= False
                    dragon2.go_left()
                    dragon2_img = pygame.image.load('dragonblu.png')
                elif e.key == pygame.K_d:
                    face_right_dragon2= True
                    dragon2.go_right()
                    dragon2_img = pygame.image.load('dragonblu1.png')
                elif e.key == pygame.K_F4:
                    playing= False
            elif e.type == pygame.KEYUP:
                if e.key in (pygame.K_a, pygame.K_d):
                    dragon2.stay()
                    
        
        screen.fill((60, 179, 113))  # background
        
        life_1 =  font.render('Vite drago verde: %d' %(dragon.LIFE), True, (255,255,255))
        screen.blit(life_1,(20,20))
        life_2 =  font.render('Vite drago blu: %d' %(dragon2.LIFE), True, (255,255,255))
        screen.blit(life_2,(480,20))
        
        arena.move_all()  # Game logic
            
        for c in arena.characters():
            if c is dragon:
                screen.blit(dragon_img, c.rect())
            elif c is dragon2:
                screen.blit(dragon2_img, c.rect())   
            elif isinstance(c, Monster):
                screen.blit(monster_img, c.rect())
            elif isinstance(c, Ball):
                screen.blit(ball_img, c.rect()) 
            else:
                pygame.draw.rect(screen, (101, 67, 33), c.rect())

        if (dragon.LIFE <=0 or dragon2.LIFE <= 0) :
            screen.blit(gameover_img, (0, 0))
            pygame.display.flip()
            time.sleep(3)
            if (dragon.LIFE <=0):
                print("Draghetto verde ha perso!")
            else :
                print("Draghetto blu ha perso!")
            pygame.quit()

        if (monster.LIFE+monster2.LIFE+monster3.LIFE+monster3.LIFE)==0:
            screen.blit(youwin_img, (0, 0))
            pygame.display.flip()
            time.sleep(3)
            print("Hai ucciso tutti i mostri, hai vinto!")
            pygame.quit()
            
        pygame.display.flip()
        clock.tick(30)
pygame.quit()

